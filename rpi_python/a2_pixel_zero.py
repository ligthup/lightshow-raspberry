'''
clear all pixels
'''

import board
import neopixel

pixel_count=300

pixels=neopixel.NeoPixel(board.D21, pixel_count)


pixels.fill((0,0,0))
# GPIO Programmierung
#!/home/pi/GPIO_Basis_1.py
# 2018-10-30
# LED ein/aus

import RPi.GPIO as GPIO
import time

# Pin-Nummern verwenden, nicht GPIO Nummern
GPIO.setmode(GPIO.BOARD)

PinNr=10

# Pin 11 - Name GPIO0
GPIO.setup(PinNr, GPIO.OUT)

# Pin 11 - Name GPIO0 einschalten - LED an
GPIO.output(PinNr,GPIO.HIGH)

# Timer oder andere Aktion machen
time.sleep(2)

# Pin 9 ausschalten
GPIO.output(PinNr,GPIO.LOW)





"""
Python/Raspberry PI
Beispiel Nr.xx
Thema: Hardwareaufbau testen
Datum: 2018-11-18
Verfasser:WM
"""

import RPi.GPIO as GPIO
import time
import random


#------------------------
def input_pins_def(p1,p2) :
    # Pin-Nummern verwenden, nicht GPIO Nummern
    GPIO.setmode(GPIO.BOARD)

    #GPIO Eingangs Pins
    GPIO.setup(p1, GPIO.IN)
    GPIO.setup(p2, GPIO.IN)

def taste_gedrueckt(PinNr):
    input = GPIO.input(PinNr)
    return input

def output_pins_def(p1,p2):
    # Pin-Nummern verwenden, nicht GPIO Nummern
    GPIO.setmode(GPIO.BOARD)

    #GPIO Ausgangs Pins
    GPIO.setup(p1, GPIO.OUT)
    GPIO.setup(p2, GPIO.OUT)

def led_schalten(PinNr,ein):
    if ein == 1 :
        GPIO.output(PinNr,GPIO.HIGH)
    else :    
        GPIO.output(PinNr,GPIO.LOW)
    
    
#------------------------

#GPIO E/A festlegen 
Starttaste = 11
Stopptaste = 13
input_pins_def(Starttaste,Stopptaste)

Startbereit = 29
Testlampe   = 31
ein = 1
aus = 0 
output_pins_def(Startbereit,Testlampe)
led_schalten(Startbereit,ein)
led_schalten(Startbereit,aus)

led_schalten(Testlampe,ein)
led_schalten(Testlampe,aus)


# Taste Start bereit ?

j=1
while  j<= 1000:  
    i= taste_gedrueckt(Starttaste)
    k= taste_gedrueckt(Stopptaste)
    
    if i == 0 :
       #print("Schalter geschlossen:",i)
        print(i,k)
    else:
       #print("Schalter       offen:",i)
       print(i,k)

    j=j+1
    

# GPIO wieder freigeben
GPIO.cleanup()
print("---- Testende -----------")



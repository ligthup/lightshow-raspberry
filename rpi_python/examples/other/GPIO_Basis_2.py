# GPIO Programmierung
#!/home/pi/GPIO_Basis_2.py
# 2018-10-30
# Taster abfragen
# PIN 1 -3.3 V = Schalte  offen
# PIN 25 0.0 V = Schalter geschlossen

import RPi.GPIO as GPIO
import time

# Pin-Nummern verwenden, nicht GPIO Nummern
GPIO.setmode(GPIO.BOARD)

PinNr=15

# Pin 15 - Name GPIO0 Eingangspin
GPIO.setup(PinNr, GPIO.IN)

input = GPIO.input(PinNr)
if input == 0 :
    print("Schalter       offen:",input)
else:
    print("Schalter geschlossen:",input)
    
# Timer oder andere Aktion machen
#time.sleep(2)

# GPIO wieder freigeben
GPIO.cleanup()





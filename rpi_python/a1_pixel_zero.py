#=======================================================#
# Date:             $time$                 #
#-------------------------------------------------------#
# Name:          S. Schaefer, A. Baumann                #
# Matr.:             #######, 2542290                   #
#-------------------------------------------------------#
# Project:             $safeprojectname$                     #
#-------------------------------------------------------#
#                                                       #
#=======================================================#



""" module to clear all pixels"""

import board
import neopixel

# Zeros Pixel input(connected pin, number of pixel on that pin)
def turn_off():
    pixel_count=300

    pixel=neopixel.NeoPixel(board.D18, pixel_count)

    pixel.fill((0,0,0)) 

    a2_pixel_zero.py
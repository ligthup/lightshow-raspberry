#=======================================================#
# Date:             $time$                 #
#-------------------------------------------------------#
# Name:          S. Schaefer, A. Baumann                #
# Matr.:             #######, 2542290                   #
#-------------------------------------------------------#
# Project:             $safeprojectname$                     #
#-------------------------------------------------------#
#                                                       #
#=======================================================#



import board
import time
import datetime
import neopixel
# import of subscript refrence
import a1_pixel_zero
import b0_clock_wakeup
import b1_sunrise
import c0_phone_ctrl
import d1_mode_1
import d2_mode_2
import d3_mode_3
import d4_color

# choose connected pin
pixel_pin = board.D18   # on V1 only D18, since B+ D19, D20, D21
#get input from phone?

# The number of NeoPixels
num_pixels = 190
# get input from phone?

brightness = input

pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness, auto_write=False)


while input()=="":
    b0_clock_wakeup

    if input()=="exit":
        break

else:
    if input()=="mode1":
        rainbow_cycle(0.005)    

    elif input()=="mode2":
        color_fade(0.01)

    elif input()=="mode3":
        strobo_light(0.001)  

    elif input()=="color":
        d4_color